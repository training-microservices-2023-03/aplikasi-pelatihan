package com.muhardin.endy.aplikasi.pelatihan.entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data @Entity
public class BatchPelatihan {
    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private LocalDate tanggalMulai;
    private LocalDate tanggalSelesai;

    @ManyToOne @JoinColumn(name = "id_pelatihan")
    private Pelatihan pelatihan;

    @ManyToOne @JoinColumn(name = "id_pengajar")
    private Pengajar pengajar;

    @ManyToMany(fetch = FetchType.LAZY) 
    @JoinTable(
        name = "peserta_batch_pelatihan",
        joinColumns = @JoinColumn(name = "id_batch_pelatihan"),
        inverseJoinColumns = @JoinColumn(name = "id_peserta")
    )
    private List<Peserta> daftarPeserta = new ArrayList<>();
}
