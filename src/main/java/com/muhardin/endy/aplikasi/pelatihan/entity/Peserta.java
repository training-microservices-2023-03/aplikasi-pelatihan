package com.muhardin.endy.aplikasi.pelatihan.entity;

import java.time.LocalDate;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Past;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data @Entity
public class Peserta {

    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @NotEmpty @Size(min = 3, max = 200)
    private String nama;

    @NotEmpty @Email @Size(max = 100)
    private String email;

    @DateTimeFormat(iso = ISO.DATE)
    @NotNull @Past
    private LocalDate tanggalLahir;
    
    @ManyToOne @JoinColumn(name = "id_kota")
    private Kota kota;

    @Enumerated(EnumType.STRING)
    private JenisKelamin jenisKelamin;

    private String foto;
    private String fotoContentType;
}
