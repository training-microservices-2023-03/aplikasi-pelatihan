package com.muhardin.endy.aplikasi.pelatihan.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.aplikasi.pelatihan.entity.Peserta;

public interface PesertaDao extends JpaRepository<Peserta, String> {
    
}
