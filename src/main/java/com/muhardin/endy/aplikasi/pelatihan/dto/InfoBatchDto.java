package com.muhardin.endy.aplikasi.pelatihan.dto;

import java.time.LocalDate;

public interface InfoBatchDto {
    LocalDate getMulai();
    LocalDate getSelesai();
    String getNamaPengajar();
    String getNamaPelatihan();
}
