package com.muhardin.endy.aplikasi.pelatihan.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.muhardin.endy.aplikasi.pelatihan.dao.KotaDao;
import com.muhardin.endy.aplikasi.pelatihan.dao.PesertaDao;
import com.muhardin.endy.aplikasi.pelatihan.entity.Kota;
import com.muhardin.endy.aplikasi.pelatihan.entity.Peserta;
import com.muhardin.endy.aplikasi.pelatihan.service.FileService;

import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Controller @Slf4j
public class RegistrasiController {

    @Autowired private PesertaDao pesertaDao;
    @Autowired private KotaDao kotaDao;

    @Autowired private FileService fileService;

    @ModelAttribute("dataKota")
    public List<Kota> dataKota(){
        return kotaDao.findAll();
    }

    @GetMapping("/registrasi/form")
    public void displayForm(@RequestParam(required = false) String nama, Model m){
        m.addAttribute("peserta", new Peserta());
        log.debug("Menampilkan form registrasi, nama : {}", nama);
    }

    @PostMapping("/registrasi/form")
    public String prosesForm(@ModelAttribute @Valid Peserta p, BindingResult errors, 
                            @RequestParam("fotoPeserta") MultipartFile fotoPeserta) throws IOException{
        log.debug("Memproses form registrasi");
        log.debug("Data Peserta : {}", p);

        log.debug("Nama file asli : {}", fotoPeserta.getOriginalFilename());
        log.debug("Tipe file : {}", fotoPeserta.getContentType());
        log.debug("Ukuran file : {}", fotoPeserta.getSize());

        String originalName = fotoPeserta.getOriginalFilename();
        String extension = ".bin";
        if(StringUtils.hasText(originalName)) {
            extension = originalName
            .substring(originalName.lastIndexOf("."));
        }

        String namafileFoto = fileService.simpan(fotoPeserta.getBytes(), extension);
        
        log.debug("Nama file foto : {}", namafileFoto);
        p.setFoto(namafileFoto);
        p.setFotoContentType(fotoPeserta.getContentType());

        if (errors.hasErrors()) {
            log.debug("Gagal validasi : {}", errors.toString());
            return "registrasi/form";
        }
        pesertaDao.save(p);
        return "redirect:sukses";
    }

    @GetMapping("/registrasi/sukses")
    public void suksesRegistrasi(){
        log.debug("Sukses registrasi");
    }
}
