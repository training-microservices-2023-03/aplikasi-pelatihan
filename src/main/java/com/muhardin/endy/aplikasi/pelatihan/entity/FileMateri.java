package com.muhardin.endy.aplikasi.pelatihan.entity;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data @Entity
public class FileMateri {
    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String namaFile;
    private String tipeFile;
    private Long ukuranFile;
    private String lokasiFile;

    @ManyToOne
    @JoinColumn(name = "id_materi")
    private Materi materi;
}
