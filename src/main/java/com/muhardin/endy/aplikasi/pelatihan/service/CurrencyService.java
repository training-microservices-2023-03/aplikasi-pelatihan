package com.muhardin.endy.aplikasi.pelatihan.service;

import java.math.BigDecimal;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import jakarta.annotation.PostConstruct;

@Service
public class CurrencyService {
    private static final String URL_CURRENCY = "/{from}/{to}.json";

    @Value("${currency.api.url}")
    private String currencyServer;

    @Autowired private WebClient.Builder webClientBuilder;

    private WebClient webClient;

    @PostConstruct
    public void setupWebclient() {
        this.webClient = webClientBuilder.baseUrl(currencyServer).build();
    }

    public BigDecimal convert(BigDecimal amount, String from, String to){
        Map<String, String> hasil = 
        webClient.get().uri(URL_CURRENCY, from, to)
        .retrieve().bodyToMono(new ParameterizedTypeReference<Map<String, String>>() {})
        .block();
        return new BigDecimal(hasil.get(to));
    }
}
