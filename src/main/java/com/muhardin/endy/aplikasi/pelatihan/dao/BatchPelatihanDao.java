package com.muhardin.endy.aplikasi.pelatihan.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.muhardin.endy.aplikasi.pelatihan.dto.InfoBatchDto;
import com.muhardin.endy.aplikasi.pelatihan.entity.BatchPelatihan;

public interface BatchPelatihanDao extends JpaRepository<BatchPelatihan, String> {

    static final String SQL_CARI_BERDASARKAN_KODE_PELATIHAN = """
            select b.* from batch_pelatihan b 
            inner join pelatihan pl on b.id_pelatihan = pl.id 
            inner join pengajar pg on b.id_pengajar = pg.id 
            where pl.kode = :kode
            """;

    static final String JPQL_INFO_BATCH = """
            select b.tanggalMulai as mulai, b.tanggalSelesai as selesai, 
            b.pengajar.nama as namaPengajar, b.pelatihan.nama as namaPelatihan 
            from BatchPelatihan b order by b.tanggalMulai
            """;

    List<BatchPelatihan> findByDaftarPesertaIsEmpty();
    List<BatchPelatihan> findByTanggalMulaiBefore(LocalDate now);

    @Query("select b from BatchPelatihan b where lower(b.pengajar.nama) like concat('%',lower(:nama),'%') ")
    List<BatchPelatihan> cariBerdasarkanNamaPengajar(@Param("nama") String nama);

    @Query(value = SQL_CARI_BERDASARKAN_KODE_PELATIHAN, nativeQuery = true)
    List<BatchPelatihan> cariBerdasarkanKodePelatihan(@Param("kode") String kodePelatihan);
    
    @Query(JPQL_INFO_BATCH)
    List<InfoBatchDto> infoBatch();


}
