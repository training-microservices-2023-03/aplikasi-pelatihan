package com.muhardin.endy.aplikasi.pelatihan.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;

@Service @Slf4j
public class FileService {

    @Value("${folder.upload}")
    private String lokasiUpload;

    @PostConstruct
    public void inisialisasiFolderUpload(){
        File uploadFolder = new File(lokasiUpload);
        log.debug("Menyiapkan lokasi upload di folder : {}", uploadFolder.getAbsolutePath());
        uploadFolder.mkdirs();
    }

    public String simpan(byte[] isiFile, String extension) throws IOException{
        String namafile = UUID.randomUUID().toString() + extension;
        Files.write(new File(lokasiUpload + File.separator + namafile).toPath(), isiFile);
        return namafile;
    }

    public byte[] ambil(String namafile) throws IOException{
        return Files.readAllBytes(new File(lokasiUpload + File.separator + namafile).toPath());
    }
}
