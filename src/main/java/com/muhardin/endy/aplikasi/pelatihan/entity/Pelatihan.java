package com.muhardin.endy.aplikasi.pelatihan.entity;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Data @Entity
public class Pelatihan {
    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    private String kode;
    private String nama;

    @OneToMany(mappedBy = "pelatihan", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Materi> daftarMateri = new ArrayList<>();
}
