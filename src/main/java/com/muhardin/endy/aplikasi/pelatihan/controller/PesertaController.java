package com.muhardin.endy.aplikasi.pelatihan.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.muhardin.endy.aplikasi.pelatihan.dao.KotaDao;
import com.muhardin.endy.aplikasi.pelatihan.dao.PesertaDao;
import com.muhardin.endy.aplikasi.pelatihan.entity.Kota;
import com.muhardin.endy.aplikasi.pelatihan.entity.Peserta;
import com.muhardin.endy.aplikasi.pelatihan.service.FileService;

import lombok.extern.slf4j.Slf4j;

@Controller @Slf4j
public class PesertaController {

    @Autowired private PesertaDao pesertaDao;
    @Autowired private KotaDao kotaDao;
    @Autowired private FileService fileService;

    @ModelAttribute("dataKota")
    public List<Kota> dataKota(){
        return kotaDao.findAll();
    }

    @GetMapping("/peserta/json")
    @ResponseBody
    public List<Peserta> dataPeserta(){
        return pesertaDao.findAll();
    }

    @GetMapping("/peserta/csv")
    public ResponseEntity<String> dataPesertaCsv(){
        StringBuilder output = new StringBuilder();
        output.append("nama,email,tanggal_lahir,kota,jenis_kelamin \r\n");

        for(Peserta p : pesertaDao.findAll()){
            output.append(p.getNama());
            output.append(",");
            output.append(p.getEmail());
            output.append(",");
            output.append(p.getTanggalLahir() == null ? "" : DateTimeFormatter.BASIC_ISO_DATE.format(p.getTanggalLahir()));
            output.append(",");
            output.append(p.getKota() == null ? "" : p.getKota().getNama());
            output.append(",");
            output.append(p.getJenisKelamin() == null ? "" : p.getJenisKelamin().name());
            output.append("\r\n");
        }

        return ResponseEntity.ok()
        //.header(HttpHeaders.CONTENT_DISPOSITION, "inline")
        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=peserta.csv")
        .contentType(MediaType.valueOf("text/csv"))
        .body(output.toString());
    }

    @GetMapping("/peserta/xls")
    public ResponseEntity<byte[]> dataPesertaXls(){
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Peserta");
            Row header = sheet.createRow(0);
            header.createCell(0).setCellValue("Nama");
            header.createCell(1).setCellValue("Email");
            header.createCell(2).setCellValue("Tanggal Lahir");
            header.createCell(3).setCellValue("Kota");
            header.createCell(4).setCellValue("Jenis Kelamin");

            Integer baris = 1;
            for(Peserta p : pesertaDao.findAll()){
                Row data = sheet.createRow(baris++);
                data.createCell(0).setCellValue(p.getNama());
                data.createCell(1).setCellValue(p.getEmail());
                data.createCell(2).setCellValue(p.getTanggalLahir() == null ? "" : DateTimeFormatter.BASIC_ISO_DATE.format(p.getTanggalLahir()));
                data.createCell(3).setCellValue(p.getKota() == null ? "" : p.getKota().getNama());
                data.createCell(4).setCellValue(p.getJenisKelamin() == null ? "" : p.getJenisKelamin().name());
            }

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            workbook.write(output);

            return ResponseEntity.ok()
            //.header(HttpHeaders.CONTENT_DISPOSITION, "inline")
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=peserta.xls")
            .contentType(MediaType.valueOf("application/vnd.ms-excel"))
            .body(output.toByteArray());
        } catch (IOException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping("/peserta/list")
    public ModelMap dataPeserta(Pageable page){
        ModelMap mm = new ModelMap();

        mm.addAttribute("dataPeserta", pesertaDao.findAll(page));

        return mm;
    }

    @GetMapping("/peserta/{id}/foto")
    public ResponseEntity<byte[]> fotoPeserta(@PathVariable String id) throws IOException{
        Optional<Peserta> p = pesertaDao.findById(id);
        if(!p.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        if(!StringUtils.hasText(p.get().getFoto())) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok().contentType(MediaType.valueOf(p.get().getFotoContentType()))
        .body(fileService.ambil(p.get().getFoto()));
    }

    @PreAuthorize("hasAuthority('EDIT_PESERTA')")
    @GetMapping("/peserta/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Peserta p){
        if(p == null) {
            p = new Peserta();
        }
        return new ModelMap()
        .addAttribute("peserta", p);
    }
}
