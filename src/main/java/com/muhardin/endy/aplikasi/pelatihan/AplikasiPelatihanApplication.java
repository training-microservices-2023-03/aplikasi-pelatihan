package com.muhardin.endy.aplikasi.pelatihan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

import nz.net.ultraq.thymeleaf.layoutdialect.LayoutDialect;

@SpringBootApplication
public class AplikasiPelatihanApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikasiPelatihanApplication.class, args);
	}

	@Bean
	public LayoutDialect layoutDialect() {
		return new LayoutDialect();
	}

	@Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }
}
