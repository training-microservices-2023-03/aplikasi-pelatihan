package com.muhardin.endy.aplikasi.pelatihan.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.aplikasi.pelatihan.entity.Pelatihan;

public interface PelatihanDao extends JpaRepository<Pelatihan, String> {
    List<Pelatihan> findByKode(String kode);
    List<Pelatihan> findByNamaContaining(String nama);

}
