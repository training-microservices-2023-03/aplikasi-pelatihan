package com.muhardin.endy.aplikasi.pelatihan.entity;

import java.time.LocalDateTime;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data @Entity
public class KehadiranPeserta {
    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne @JoinColumn(name = "id_sesi_pelatihan")
    private SesiPelatihan sesiPelatihan;

    @ManyToOne @JoinColumn(name = "id_peserta")
    private Peserta peserta;

    private LocalDateTime waktuDatang;
    private LocalDateTime waktuPulang;
}
