package com.muhardin.endy.aplikasi.pelatihan.controller;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.muhardin.endy.aplikasi.pelatihan.dao.PesertaDao;
import com.muhardin.endy.aplikasi.pelatihan.entity.Peserta;

import jakarta.validation.Valid;

@RestController
public class PesertaApiController {

    @Autowired private PesertaDao pesertaDao;

    @GetMapping("/api/peserta/")
    public List<Peserta> dataPeserta(){
        return pesertaDao.findAll();
    }

    @PostMapping("/api/peserta/")
    @ResponseStatus(HttpStatus.CREATED)
    public void simpanPesertaBaru(@RequestBody @Valid Peserta p){
        pesertaDao.save(p);
    }

    @PutMapping("/api/peserta/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void updatePeserta(@PathVariable("id") Peserta pesertaDatabase, @RequestBody @Valid Peserta pesertaUpdate){
        BeanUtils.copyProperties(pesertaUpdate, pesertaDatabase, "id");
        pesertaDao.save(pesertaDatabase);
    }

    @DeleteMapping("/api/peserta/{id}")
    public void deletePeserta(@PathVariable("id") String id){
        pesertaDao.deleteById(id);
    }
}
