package com.muhardin.endy.aplikasi.pelatihan.entity;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.annotations.GenericGenerator;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.Data;

@Data @Entity
public class Materi {
    @Id 
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @ManyToOne
    @JoinColumn(name = "id_pelatihan")
    private Pelatihan pelatihan;

    private Integer urutan;
    private String judul;
    private String keterangan;
    private Integer durasiJam;

    @OneToMany(mappedBy = "materi", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FileMateri> daftarFileMateri = new ArrayList<>();
}
