package com.muhardin.endy.aplikasi.pelatihan.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.muhardin.endy.aplikasi.pelatihan.entity.Kota;

public interface KotaDao extends JpaRepository<Kota, String> {

}
