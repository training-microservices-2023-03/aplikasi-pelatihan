create table kota (
    id varchar(36),
    nama varchar(200) not null,
    primary key (id)
);

alter table peserta 
add column id_kota varchar(36);

alter table peserta 
add constraint fk_peserta_kota foreign key (id_kota) references kota(id);

alter table peserta 
add column jenis_kelamin varchar(10);

alter table peserta
add column tanggal_lahir date;