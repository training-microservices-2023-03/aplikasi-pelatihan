-- password = abcd
insert into app_users (id, username, hashed_password, active) values 
('u001', 'user001', '{bcrypt}$2a$12$kJYjbJ3vi1KyFEP8V31GMeKa7lSZXMo7cClH2I0NtQwuutwPNJhSC', true);

insert into app_users (id, username, hashed_password, active) values 
('u002', 'user002', '{noop}rahasia123', true);

insert into app_permissions (id, permission_label, permission_value) values 
('p001', 'View Peserta', 'VIEW_PESERTA');

insert into app_permissions (id, permission_label, permission_value) values 
('p002', 'Edit Peserta', 'EDIT_PESERTA');

insert into app_users_permissions (id_user, id_permission) values
('u001', 'p001');

insert into app_users_permissions (id_user, id_permission) values
('u001', 'p002');

insert into app_users_permissions (id_user, id_permission) values
('u002', 'p001');
