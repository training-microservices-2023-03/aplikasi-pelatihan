create table peserta (
    id varchar(36),
    nama varchar(200) not null, 
    email varchar(100) not null,
    primary key (id),
    unique (email)
);

create table pengajar (
    id varchar(36),
    nama varchar(200) not null, 
    email varchar(100) not null,
    primary key (id)
);

create table pelatihan (
    id varchar(36),
    id_pengajar varchar(36),
    kode varchar(100) not null,
    nama varchar(200) not null, 
    primary key (id),
    foreign key (id_pengajar) references pengajar(id)
);

create table materi (
    id varchar(36),
    id_pelatihan varchar(36) not null,
    urutan integer not null,
    judul varchar(100) not null,
    keterangan varchar(255),
    durasi_jam integer not null,
    primary key (id),
    foreign key (id_pelatihan) references pelatihan(id)
);

create table file_materi (
    id varchar(36),
    id_materi varchar(36) not null,
    nama_file varchar(100) not null,
    tipe_file varchar(50) not null,
    ukuran_file bigint not null,
    lokasi_file varchar(100) not null,
    primary key (id), 
    foreign key (id_materi) references materi(id)
);

create table batch_pelatihan (
    id varchar(36),
    id_pelatihan varchar(36) not null,
    id_pengajar varchar(36) not null,
    tanggal_mulai date not null,
    tanggal_selesai date not null,
    primary key (id),
    foreign key (id_pelatihan) references pelatihan(id),
    foreign key (id_pengajar) references pengajar(id)
);

create table peserta_batch_pelatihan (
    id_batch_pelatihan varchar(36) not null,
    id_peserta varchar(36) not null,
    primary key (id_batch_pelatihan, id_peserta),
    foreign key (id_batch_pelatihan) references batch_pelatihan(id),
    foreign key (id_peserta) references peserta(id)
);

create table sesi_pelatihan (
    id varchar(36),
    id_batch_pelatihan varchar(36) not null,
    waktu_mulai timestamp(6) without time zone,
    waktu_selesai timestamp(6) without time zone,
    primary key (id),
    foreign key (id_batch_pelatihan) references batch_pelatihan(id)
);

create table kehadiran_peserta (
    id varchar(36),
    id_sesi_pelatihan varchar(36) not null,
    id_peserta varchar(36) not null,
    waktu_datang timestamp(6) without time zone,
    waktu_pulang timestamp(6) without time zone,
    primary key (id),
    foreign key (id_sesi_pelatihan) references sesi_pelatihan(id),
    foreign key (id_peserta) references peserta(id)
);