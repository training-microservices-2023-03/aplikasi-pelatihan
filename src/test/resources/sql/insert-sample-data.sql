insert into pelatihan (id, kode, nama) values 
('p001', 'P-001', 'Pelatihan 001'),
('p002', 'P-002', 'Pelatihan 002'),
('p003', 'P-003', 'Pelatihan 003'),
('p004', 'P-004', 'Pelatihan 004'),
('p005', 'P-005', 'Pelatihan 005'),
('p006', 'P-006', 'Pelatihan 006'),
('p007', 'P-007', 'Pelatihan 007'),
('p008', 'P-008', 'Pelatihan 008'),
('p009', 'P-009', 'Pelatihan 009'),
('p010', 'P-010', 'Pelatihan 010'),
('p011', 'P-011', 'Pelatihan 011');

insert into peserta (id, nama, email) values 
('s001', 'Student 001', 'student001@yopmail.com'),
('s002', 'Student 002', 'student002@yopmail.com'),
('s003', 'Student 003', 'student003@yopmail.com'),
('s004', 'Student 004', 'student004@yopmail.com');

insert into pengajar (id, nama, email) values 
('i001', 'Instruktur 001', 'instruktur001@yopmail.com'),
('i002', 'Instruktur 002', 'instruktur002@yopmail.com'),
('i003', 'Instruktur 003', 'instruktur003@yopmail.com'),
('i004', 'Instruktur 004', 'instruktur004@yopmail.com');

insert into materi (id, id_pelatihan, urutan, judul, keterangan, durasi_jam) values
('m101', 'p001', 1, 'M-001', 'Materi Pelatihan 001', 4),
('m102', 'p001', 2, 'M-002', 'Materi Pelatihan 002', 2),
('m103', 'p001', 3, 'M-003', 'Materi Pelatihan 003', 3),
('m104', 'p001', 4, 'M-004', 'Materi Pelatihan 004', 1),
('m105', 'p001', 5, 'M-005', 'Materi Pelatihan 005', 5),
('m106', 'p001', 6, 'M-006', 'Materi Pelatihan 006', 3);

insert into batch_pelatihan (id, id_pelatihan, id_pengajar, tanggal_mulai, tanggal_selesai) values 
('b001', 'p001', 'i001', '2023-10-01', '2023-10-05');