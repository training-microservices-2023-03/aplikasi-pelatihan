package com.muhardin.endy.aplikasi.pelatihan.service;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CurrencyServiceTests {
    @Autowired private CurrencyService currencyService;

    @Test
    public void testUsdToIdr(){
        BigDecimal idr = currencyService.convert(BigDecimal.ONE, "usd", "idr");
        System.out.println("US$1 => Rp. "+idr+"");
    }
}
