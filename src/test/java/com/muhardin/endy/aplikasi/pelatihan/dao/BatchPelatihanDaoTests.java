package com.muhardin.endy.aplikasi.pelatihan.dao;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.transaction.annotation.Transactional;

import com.muhardin.endy.aplikasi.pelatihan.dto.InfoBatchDto;
import com.muhardin.endy.aplikasi.pelatihan.entity.BatchPelatihan;
import com.muhardin.endy.aplikasi.pelatihan.entity.Pelatihan;
import com.muhardin.endy.aplikasi.pelatihan.entity.Pengajar;
import com.muhardin.endy.aplikasi.pelatihan.entity.Peserta;

@SpringBootTest
@Sql(
    scripts = {"classpath:/sql/delete-sample-data.sql", "classpath:/sql/insert-sample-data.sql"}
)
public class BatchPelatihanDaoTests {
    @Autowired
    private BatchPelatihanDao batchPelatihanDao;

    @Autowired 
    private PelatihanDao pelatihanDao;

    @Test
    public void testInsert(){
        // relasi yang dibutuhkan
        Pelatihan i1 = pelatihanDao.findById("p002").get();

        // relasi tidak harus query dari db, yang diperlukan hanya ID saja
        Pengajar pengajar = new Pengajar();
        pengajar.setId("i002");

        BatchPelatihan bp = new BatchPelatihan();
        bp.setPelatihan(i1);
        bp.setPengajar(pengajar);
        bp.setTanggalMulai(LocalDate.of(2023, 01, 01));
        bp.setTanggalSelesai(LocalDate.of(2023, 02, 01));

        // peserta pelatihan
        Peserta s1 = new Peserta();
        s1.setId("s001");
        bp.getDaftarPeserta().add(s1);

        Peserta s3 = new Peserta();
        s3.setId("s003");
        bp.getDaftarPeserta().add(s3);
        
        batchPelatihanDao.save(bp);
    }

    // penjelasan tentang transactional bisa dilihat 
    // di sini : https://www.youtube.com/playlist?list=PL9oC_cq7OYbwP3r01vwXEN_QQn9p_aZHt
    // Episode 6 - 11
    @Test @Transactional @Rollback(false)
    public void testTambahPesertaBatch(){
        BatchPelatihan bp = batchPelatihanDao.findById("b001").get();

        // peserta pelatihan
        Peserta s2 = new Peserta();
        s2.setId("s002");
        bp.getDaftarPeserta().add(s2);

        Peserta s4 = new Peserta();
        s4.setId("s004");
        bp.getDaftarPeserta().add(s4);
        
        batchPelatihanDao.save(bp);
    }

    @Test
    public void testCariBatchYangSudahMulai(){
        List<BatchPelatihan> sudahMulai = batchPelatihanDao.findByTanggalMulaiBefore(LocalDate.now());
        System.out.println("Jumlah hasil : "+sudahMulai.size());
    }

    @Test
    public void testCariBatchYangTidakAdaPesertanya(){
        List<BatchPelatihan> tanpaPeserta = batchPelatihanDao.findByDaftarPesertaIsEmpty();
        System.out.println("Jumlah batch yang kosong : "+tanpaPeserta.size());
    }

    @Test
    public void testCariBatchBerdasarkanNamaPengajar(){
        List<BatchPelatihan> batchNamaPengajar = batchPelatihanDao.cariBerdasarkanNamaPengajar("001");
        System.out.println("Jumlah hasil query : "+batchNamaPengajar.size());
    }

    @Test
    public void testCariBatchBerdasarkanKodePelatihan(){
        List<BatchPelatihan> batchKodePelatihan = batchPelatihanDao.cariBerdasarkanKodePelatihan("P-001");
        System.out.println("Jumlah hasil query : "+batchKodePelatihan.size());
    }

    @Test
    public void testInfoBatch(){
        List<InfoBatchDto> hasil = batchPelatihanDao.infoBatch();
        for(InfoBatchDto info : hasil){
            System.out.println("Nama Pelatihan : "+info.getNamaPelatihan());
            System.out.println("Nama Pengajar : "+info.getNamaPengajar());
            System.out.println("Mulai : "+info.getMulai());
            System.out.println("Selesai : "+info.getSelesai());
        }
    }
}
