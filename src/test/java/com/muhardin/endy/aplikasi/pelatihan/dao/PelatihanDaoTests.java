package com.muhardin.endy.aplikasi.pelatihan.dao;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.jdbc.Sql;

import com.muhardin.endy.aplikasi.pelatihan.entity.Pelatihan;

@SpringBootTest
@Sql(
    scripts = {"classpath:/sql/delete-sample-data.sql", "classpath:/sql/insert-sample-data.sql"}
)
public class PelatihanDaoTests {
    @Autowired private PelatihanDao pelatihanDao;
    @Autowired private BatchPelatihanDao batchPelatihanDao;

    @Test
    public void testInsertPelatihan(){
        Pelatihan p = new Pelatihan();
        p.setKode("J-002");
        p.setNama("Java Advanced");

        pelatihanDao.save(p);
        
    }

    @Test
    public void testSelectPelatihan(){
        Optional<Pelatihan> op01 = pelatihanDao.findById("p001");
        Assertions.assertTrue(op01.isPresent());
        System.out.println("Kode Pelatihan : "+op01.get().getKode());

        Assertions.assertTrue(pelatihanDao.findById("p9999").isEmpty());

        Long jumlahPelatihan = pelatihanDao.count();
        System.out.println("Jumlah pelatihan : "+jumlahPelatihan);

        List<Pelatihan> semuaData = pelatihanDao.findAll();
        for(Pelatihan p : semuaData){
            System.out.println(p.getNama());
        }

        Page<Pelatihan> pagingPelatihan = pelatihanDao.findAll(PageRequest.of(1, 10));
        System.out.println("Halaman saat ini : "+pagingPelatihan.getNumber());
        System.out.println("Jumlah record di halaman ini : "+pagingPelatihan.getNumberOfElements());
        System.out.println("Jumlah page : "+pagingPelatihan.getTotalPages());
        System.out.println("Jumlah record total : "+pagingPelatihan.getTotalElements());
    }

    @Test
    public void testDeletePelatihanBerelasi(){
        batchPelatihanDao.deleteById("b001");
        pelatihanDao.deleteById("p001");
    }
}
