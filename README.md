# Aplikasi Pelatihan #

[![Arsitektur Aplikasi](img/skema-aplikasi.png)](img/skema-aplikasi.png)

[![Dependensi Spring Boot](img/dependensi-spring-boot.png)](img/dependensi-spring-boot.png)

Daftar fitur :

* Registrasi Peserta
* Upload foto
* Daftar Training
* Download XLS, PDF

## Setup Database ##

* Nama database : `db-pelatihan`
* Username database : `pelatihan`
* Password database : `pelatihan123`

Connect ke database

```
psql -h 127.0.0.1 -U pelatihan -d db-pelatihan
```

## Skema Database ##

[![Skema database](img/skema-database-pelatihan.png)](img/skema-database-pelatihan.png)


## Cara Menjalankan Aplikasi ##

1. Run database development

    ```
    docker compose up
    ```

2. Run Aplikasi

    ```
    mvn clean spring-boot:run
    ```

3. Browse ke http://localhost:8080/


## Fitur Aplikasi ##

* Registrasi Peserta
* Master data jenis pelatihan
* Detail materi untuk pelatihan
* Upload materi pelatihan
* Download materi pelatihan
* Peserta ikut pelatihan
* Input kehadiran pelatihan

## Referensi Tambahan ##


* [Authentication API dengan OAuth](https://www.youtube.com/watch?v=jSbMvRxQDpc)